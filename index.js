let post =[];
let count = 1;

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();
	post.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	count++;
	console.log(post);
	showpost(post);
	alert('Successfully Added');
})

const showpost = (post) => {
	let postEntries = '';
	post.forEach((post) => {
		postEntries += `
		<div id='post-${post.id}'>
			<h3 id='post-title-${post.id}'>${post.title}</h3>
			<p id='post-body-${post.id}'>${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>`
		;
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

const editPost = (postId) => {
	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();
	for(let i=0; i<post.length; i++){
		if (post[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			post[i].title = document.querySelector('#txt-edit-title').value;
			post[i].body = document.querySelector('#txt-edit-body').value;

			showpost(post);
			alert("Successfully Updated");
			break;
		}
	}
})

const deletePost = (a) => {
	console.log(a);
	let index;
	index - 1;
	post.forEach(i => {
		if (a == i.id) {
			console.log(true);
			index = post.indexOf(i);
		}
	})
	console.log(index);
	post.splice(index, 1);
	document.querySelector(`#post-${a}`).remove();
	console.log(post);
};